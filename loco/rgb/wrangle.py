import json
import pandas as pd

train_files = ['loco-sub2-v1-train.json', 'loco-sub3-v1-train.json', 'loco-sub5-v1-train.json']
val_files = ['loco-sub1-v1-val.json', 'loco-sub4-v1-val.json']


def label_wrangle(files, file_type):
    for file in files:
        f = open(file)
        j = json.load(f)

        df_annotations = pd.DataFrame.from_dict(j["annotations"])
        df_annotations = df_annotations[["image_id", "bbox", "category_id"]]

        df_categories = pd.DataFrame.from_dict(j["categories"])
        df_categories["category_id"] = df_categories["id"]
        df_categories = df_categories[["category_id", "name"]]

        df_cat_anno = pd.merge(df_categories, df_annotations, on='category_id')

        df_images = pd.DataFrame.from_dict(j["images"])
        df_images["image_id"] = df_images["id"]
        df_images = df_images[["image_id", "width", "height", "file_name"]]
        df_images["file_name"] = df_images["file_name"].apply(lambda fn: fn[:-3] + "txt")

        df = pd.merge(df_cat_anno, df_images, on='image_id')
        df = df[["image_id", "name","bbox", "width", "height", "file_name"]]
        class_dict = {'forklift': 0,'pallet_truck': 1,'pallet': 2,'small_load_carrier': 3,'stillage': 4}
        df.name = [class_dict[item] for item in df.name]

        path_to_annotations = '/usr/src/workspace/loco/dataset/labels/{}/'.format(file_type)

        for idx, row in df.iterrows():
            _id = row["image_id"]
            _class = row["name"]
            _bbox = row["bbox"]
            _file_name = row["file_name"]
            bb_width = _bbox[2]
            bb_height = _bbox[3]
            x = _bbox[0]
            y = _bbox[1]
            x_center = x + (bb_width/2)
            y_center = y + (bb_height/2)
            width = row["width"]
            height = row["height"]
            n_x_center = x_center/width
            n_y_center = y_center/height
            n_width = bb_width/width
            n_height = bb_height/height

            file_name = row["file_name"]
            line_to_write  = "{} {} {} {} {}\n".format(_class, n_x_center, n_y_center, n_width, n_height)
            with open(path_to_annotations + file_name, "a") as outH:
                outH.write(line_to_write)
                
label_wrangle(train_files,'train')
label_wrangle(val_files,'val')